from reinforcement import coef
from training import number_of_steps


def get_sequence_actions(initial_annotation, human_annotation):
    counter_total, counter_right, counter_left, counter_top, counter_down,\
        counter_wider, counter_taller, counter_fatter, counter_thinner = 0, 0, 0, 0, 0, 0, 0, 0, 0

    xmin_i, xmax_i, ymin_i, ymax_i = initial_annotation[0][1], initial_annotation[0][2], \
                                     initial_annotation[0][3], initial_annotation[0][4]

    width_i = xmax_i - xmin_i
    height_i = ymax_i - ymin_i

    xmin_h, xmax_h, ymin_h, ymax_h = human_annotation[0][1], human_annotation[0][2], \
                                     human_annotation[0][3], human_annotation[0][4]

    width_h = xmax_h - xmin_h
    height_h = ymax_h - ymin_h

    # compute center of initial annotation
    xcen_i = (xmax_i - xmin_i) / 2 + xmin_i
    ycen_i = (ymax_i - ymin_i) / 2 + ymin_i

    # compute center of human annotation
    xcen_h = (xmax_h - xmin_h) / 2 + xmin_h
    ycen_h = (ymax_h - ymin_h) / 2 + ymin_h

    margin = coef/2

    # movement actions
    while counter_total < number_of_steps:
        counter_total += 1
        if xcen_i < xcen_h - margin*width_i:
            counter_right += 1
            xcen_i += coef * width_i
        elif xcen_i > xcen_h + margin*width_i:
            counter_left += 1
            xcen_i -= coef * width_i
        elif ycen_i > ycen_h + margin*height_i:
            counter_top += 1
            ycen_i -= coef * height_i
        elif ycen_i < ycen_h - margin*height_i:
            counter_down += 1
            ycen_i += coef * height_i
        elif width_i < width_h - margin*width_i:
            counter_wider += 1
            width_i += coef * width_i
        elif width_i > width_h + margin*width_i:
            counter_thinner += 1
            width_i -= coef * width_i
        elif height_i < height_h - margin*height_i:
            counter_taller += 1
            height_i += coef * height_i
        elif height_i > height_h + margin*height_i:
            counter_fatter += 1
            height_i -= coef * height_i

    return [counter_top, counter_down, counter_left, counter_right, counter_wider,
            counter_taller, counter_fatter, counter_thinner]
