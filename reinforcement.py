from features import *
import numpy as np

HOG = 1
CORNER = 2
CONV = 3
CONV_RES = 4
MY_CONV = 5
# Different actions that the agent can do
number_of_actions = 9
# Actions captures in the history vector
actions_of_history = 9

coef = 0.1  # for left, right, top, down actions
coef_ratio = coef/2  # for bigger, smaller, fatter, taller actions


def perform_movement_action(image_shape, region_image, action, bbox_ul, bbox_lr, original_image):
    region_mask = np.zeros(image_shape)
    height = region_image.shape[0]
    width = region_image.shape[1]

    if action == 1:  # up
        bbox_ul = (int(bbox_ul[0] - coef * height), bbox_ul[1])
        bbox_lr = (int(bbox_lr[0] - coef * height), bbox_lr[1])
    elif action == 2:  # down
        bbox_ul = (int(bbox_ul[0] + coef * height), bbox_ul[1])
        bbox_lr = (int(bbox_lr[0] + coef * height), bbox_lr[1])
    elif action == 3:  # left
        bbox_ul = (bbox_ul[0], int(bbox_ul[1] - coef * width))
        bbox_lr = (bbox_lr[0], int(bbox_lr[1] - coef * width))
    elif action == 4:  # right
        bbox_ul = (bbox_ul[0], int(bbox_ul[1] + coef * width))
        bbox_lr = (bbox_lr[0], int(bbox_lr[1] + coef * width))
    elif action == 5:  # wider
        bbox_ul = (bbox_ul[0], int(bbox_ul[1] - coef_ratio * width))
        bbox_lr = (bbox_lr[0], int(bbox_lr[1] + coef_ratio * width))
    elif action == 6:  # taller
        bbox_ul = (int(bbox_ul[0] - coef_ratio * height), bbox_ul[1])
        bbox_lr = (int(bbox_lr[0] + coef_ratio * height), bbox_lr[1])
    elif action == 7:  # fatter
        bbox_ul = (int(bbox_ul[0] + coef_ratio * height), bbox_ul[1])
        bbox_lr = (int(bbox_lr[0] - coef_ratio * height), bbox_lr[1])
    elif action == 8:  # thinner
        bbox_ul = (bbox_ul[0], int(bbox_ul[1] + coef_ratio * width))
        bbox_lr = (bbox_lr[0], int(bbox_lr[1] - coef_ratio * width))

    # boundaries in case bbox exceeds image size
    if bbox_lr[0] > image_shape[0]:
        bbox_lr = (image_shape[0], bbox_lr[1])
    if bbox_lr[1] > image_shape[1]:
        bbox_lr = (bbox_lr[0], image_shape[1])
    if bbox_ul[0] < 0:
        bbox_ul = (0, bbox_ul[1])
    if bbox_ul[1] < 0:
        bbox_ul = (bbox_ul[0], 0)

    region_image = original_image[int(bbox_ul[0]):int(bbox_lr[0]),
                                  int(bbox_ul[1]):int(bbox_lr[1])]
    region_mask[int(bbox_ul[0]):int(bbox_lr[0]),
                int(bbox_ul[1]):int(bbox_lr[1])] = 1

    return region_mask, region_image, bbox_ul, bbox_lr


def update_history_vector(history_vector, action):
    action_vector = np.zeros(number_of_actions)
    action_vector[action-1] = 1
    size_history_vector = np.size(np.nonzero(history_vector)) # max 4 actions stored in hist vector (9 values for each action, 1 of them nonzero)
    updated_history_vector = np.zeros(number_of_actions*actions_of_history)
    if size_history_vector < actions_of_history:
        aux2 = 0
        for l in range(number_of_actions*size_history_vector, number_of_actions*size_history_vector+number_of_actions):
            history_vector[l] = action_vector[aux2]
            aux2 += 1
        return history_vector
    else:
        for j in range(0, number_of_actions*(actions_of_history-1) - 1):
            updated_history_vector[j] = history_vector[j+number_of_actions]
        aux = 0
        for k in range(number_of_actions*(actions_of_history-1), number_of_actions*actions_of_history):
            updated_history_vector[k] = action_vector[aux]
            aux += 1
        return updated_history_vector


def get_state(region_image, history_vector, model, state_type, image, bbox_ul, bbox_lr, visual_descriptor_size):
    coef = 0.2
    height = region_image.shape[0]
    width = region_image.shape[1]

    if state_type == HOG:
        descriptor_image = get_HOG_image_descriptor_for_image(region_image)
    elif state_type == CORNER:
        descriptor_image = get_CORNER_image_descriptor_for_image(region_image)
    elif state_type == CONV:
        descriptor_image = get_conv_image_descriptor_for_image(region_image, model)
    elif state_type == CONV_RES:
        descriptor_image = get_res_image_descriptor(region_image, model)

    history_vector = np.reshape(history_vector, (number_of_actions * actions_of_history, 1))
    descriptor_image = np.reshape(descriptor_image, (visual_descriptor_size, 1))
    state = np.vstack((descriptor_image, history_vector))

    return state


def get_reward_movement(iou, new_iou):
    if new_iou > iou:
        reward = 1
    else:
        reward = -3
    return reward


def get_reward_trigger(new_iou, iou_threshold, step, max_steps):
    step_discount_factor = (max_steps - step)/max_steps
    if np.round(new_iou, 2) >= iou_threshold:
        reward = + 6 + 4*step_discount_factor
    else:
        reward = -3
    return reward


def get_reward_stop(iou, iou_threshold):
    if np.round(iou, 2) >= iou_threshold:
        return 1
    return 0
