from image_helper import *
from parse_annotations import *
from reinforcement import *
from metrics import *
from visualization import *
from xlsx_saver import XlsxFile
from PIL import ImageDraw
from linUCB import LinucbAgent

iou_threshold = 0.85
class_object = 3
number_of_steps = 10
state_type = HOG
bool_draw = 0

folder_skews = 'skews_bigger'
retinanet = False
path_model = '/home/user/morgane/drl-agent/models_image_zooms'
path_testing_folder = '/home/user/morgane/drl-agent/testing_visualizations'
path_dataset = '/home/user/morgane/datasets/verb_middle_200_test'

if __name__ == "__main__":

    pretrained_model = None
    visual_descriptor_size = 0
    if state_type == CONV:
        pretrained_model = obtain_compiled_vgg_16()
        visual_descriptor_size = 25088
    elif state_type == CONV_RES:
        pretrained_model = obtain_compiled_resnet50()
        visual_descriptor_size = 2048
    elif state_type == HOG:
        visual_descriptor_size = 2916
    elif state_type == CORNER:
        visual_descriptor_size = 1024
    else:
        raise ValueError('Unknown state type.')
    state_size = visual_descriptor_size + actions_of_history*number_of_actions

    path_agent = path_model + '/model.pickle'

    linucb = LinucbAgent(path_agent)

    # load image names
    image_names = np.array([load_images_names_in_data_set('test', path_dataset)])

    images = get_all_images(image_names, path_dataset)

    xlsx_file = XlsxFile(path_testing_folder)

    for j in range(np.size(image_names)):
        image = np.array(images[j])
        image_name = image_names[0][j]

        # ground-truth bounding-box
        annotation = get_bb_from_annotation(image_name, path_dataset + '/annotations/') # 2D array, one array for each object (class number + coordinates)
        # skewed bounding-box
        skewed_annotation = get_bb_from_annotation(image_name, path_dataset + '/' + folder_skews + '/')

        print("Image name: ", image_name)

        background = Image.new('RGBA', (5500, 500), (255, 255, 255, 255))
        draw = ImageDraw.Draw(background)

        # 3D array, every layer corresponds to one object in the image, area of objects masked (=1)
        gt_masks = generate_bounding_box_from_annotation(annotation, image.shape)
        # array of class id of every object in image
        array_classes_gt_objects = get_ids_objects_from_annotation(annotation)

        # Iterate through all the objects in the ground truth of an image
        for k in range(np.size(array_classes_gt_objects)):
            # We check whether the ground truth object is of the target class category
            if array_classes_gt_objects[k] == class_object:
                gt_mask = gt_masks[:, :, k]
                region_image = image[skewed_annotation[k][3]:skewed_annotation[k][4],
                                     skewed_annotation[k][1]:skewed_annotation[k][2]]
                region_mask = np.zeros([image.shape[0], image.shape[1]])
                region_mask[skewed_annotation[k][3]:skewed_annotation[k][4],
                            skewed_annotation[k][1]:skewed_annotation[k][2]] = 1
                bbox_ul = (skewed_annotation[k][3], skewed_annotation[k][1])
                bbox_lr = (skewed_annotation[k][4], skewed_annotation[k][2])
                # calculate iou
                iou = follow_iou(gt_mask, region_mask)
                new_iou = iou
                initial_iou = iou
                if bool_draw:
                    background = draw_sequences(0, 0, draw, region_image, background, path_testing_folder, initial_iou,
                                                initial_iou, image_name)
                # init of the history vector that indicates past actions (9 actions * 4 steps in the memory)
                history_vector = np.zeros([number_of_actions * actions_of_history])
                # computation of the initial state
                state = get_state(region_image, history_vector, pretrained_model, state_type, image, bbox_ul,
                                  bbox_lr, visual_descriptor_size)
                action = 0
                done = False
                step = 0
                while (step < number_of_steps) and (not done):
                    category = int(array_classes_gt_objects[k] - 1)
                    step += 1

                    predicted_action = linucb.predict_action(state.T)

                    if step == number_of_steps:
                        action = 9
                    else:
                        action = predicted_action

                    # terminal action
                    if action == 9:
                        done = True
                    else:
                        region_mask, region_image, bbox_ul, bbox_lr = perform_movement_action(
                            (image.shape[0], image.shape[1]),
                            region_image, action,
                            bbox_ul, bbox_lr,
                            image)
                        new_iou = follow_iou(gt_mask, region_mask)
                        done = False

                    if step == 1:
                        initial_iou = iou
                        max_iou = iou
                    max_iou = max(max_iou, new_iou)
                    iou = new_iou

                    history_vector = update_history_vector(history_vector, action)
                    new_state = get_state(region_image, history_vector, pretrained_model, state_type, image,
                                          bbox_ul, bbox_lr, visual_descriptor_size)
                    state = new_state

                    if bool_draw:
                        background = draw_sequences(step, action, draw, region_image, background, path_testing_folder,
                                                    iou, initial_iou, image_name)

                    if done:
                        generate_annotation_from_bounding_box(class_object, image_name, bbox_ul, bbox_lr, path_testing_folder)
                        xlsx_file.write_to_xlsx_file(image_name, np.round(initial_iou, 2), np.round(iou, 2),
                                                     np.round(max_iou, 2), iou_threshold, step, predicted_action, 0)
    xlsx_file.on_testing_end()
