import xlsxwriter


class XlsxFile:
    def __init__(self, path_testing):
        self.workbook = xlsxwriter.Workbook(path_testing + '/test_results.xlsx')
        self.worksheet = self.workbook.add_worksheet()
        self.worksheet.write(0, 0, 'Image name')
        self.worksheet.write(0, 1, 'Initial IoU')
        self.worksheet.write(0, 2, 'Final IoU')
        self.worksheet.write(0, 3, 'Max IoU')
        self.worksheet.write(0, 4, 'Number of steps')
        self.worksheet.write(0, 5, 'STOP Q-value')
        self.worksheet.write(0, 6, 'Reason for episode end')
        self.worksheet.write(0, 8, 'Image name')
        self.worksheet.write(0, 9, 'Initial IoU')
        self.worksheet.write(0, 10, 'Final IoU')
        self.worksheet.write(0, 11, 'Max IoU')
        self.worksheet.write(0, 12, 'Number of steps')
        self.worksheet.write(0, 13, 'STOP Q-value')
        self.worksheet.write(0, 14, 'Reason for episode end')
        self.row_good = 0
        self.row_bad = 0
        self.good_detections_and_stop = 0
        self.bad_detections_and_stop = 0
        self.good_final_equal_max = 0
        self.bad_final_equal_max = 0

    def write_to_xlsx_file(self, image_name, inital_iou, final_iou, max_iou, iou_threshold,
                           steps, last_predicted_action, stop_q_value):
        row = 0
        if final_iou >= inital_iou:
            col = 0
            self.row_good += 1
            row = self.row_good
        else:
            col = 8
            self.row_bad += 1
            row = self.row_bad
        self.worksheet.write(row, col, image_name)
        self.worksheet.write(row, col + 1, inital_iou)
        self.worksheet.write(row, col + 2, final_iou)
        self.worksheet.write(row, col + 3, max_iou)
        if (max_iou == final_iou) & (final_iou >= inital_iou):
            self.good_final_equal_max += 1
        elif (max_iou == final_iou) & (final_iou < inital_iou):
            self.bad_final_equal_max += 1
        self.worksheet.write(row, col + 4, steps)
        self.worksheet.write(row, col + 5, stop_q_value)
        if last_predicted_action == 9:
            self.worksheet.write(row, col + 6, 'STOP q-value')
            if final_iou >= inital_iou:
                self.good_detections_and_stop += 1
            else:
                self.bad_detections_and_stop += 1
        else:
            self.worksheet.write(row, col + 6, 'max number of steps')

    def on_testing_end(self):
        start_row = max(self.row_good + 1, self.row_bad + 1)
        # good detections
        self.worksheet.write(start_row, 0, 'Average')
        self.worksheet.write(start_row, 2, '=AVERAGE(C2:C' + str(self.row_good + 1) + ')')
        self.worksheet.write(start_row, 3, '=AVERAGE(D2:D' + str(self.row_good + 1) + ')')
        self.worksheet.write(start_row, 4, '=AVERAGE(E2:E' + str(self.row_good + 1) + ')')
        self.worksheet.write(start_row, 5, '=AVERAGE(F2:F' + str(self.row_good + 1) + ')')

        num_good_detections = self.row_good
        start_row += 2
        self.worksheet.write(start_row, 0, 'Number of good detections')
        self.worksheet.write(start_row, 1, num_good_detections)
        start_row += 1
        self.worksheet.write(start_row, 0, 'Good detections + STOP')
        self.worksheet.write(start_row, 1, self.good_detections_and_stop)
        start_row += 1
        self.worksheet.write(start_row, 0, 'Final IoU == Max IoU')
        self.worksheet.write(start_row, 1, self.good_final_equal_max)
        start_row += 1
        self.worksheet.write(start_row, 0, 'Average change in IoU')
        self.worksheet.write(start_row, 1, '=(SUM(C2:C' + str(self.row_good + 1) + ')-SUM(B2:B' + str(self.row_good + 1) + '))/' + str(num_good_detections))

        start_row = max(self.row_good + 1, self.row_bad + 1)
        # bad detections
        self.worksheet.write(start_row, 8, 'Average')
        self.worksheet.write(start_row, 10, '=AVERAGE(K2:K' + str(self.row_bad + 1) + ')')
        self.worksheet.write(start_row, 11, '=AVERAGE(L2:L' + str(self.row_bad + 1) + ')')
        self.worksheet.write(start_row, 12, '=AVERAGE(M2:M' + str(self.row_bad + 1) + ')')
        self.worksheet.write(start_row, 13, '=AVERAGE(N2:N' + str(self.row_bad + 1) + ')')

        num_bad_detections = self.row_bad
        start_row += 2
        self.worksheet.write(start_row, 8, 'Number of bad detections')
        self.worksheet.write(start_row, 9, num_bad_detections)
        start_row += 1
        self.worksheet.write(start_row, 8, 'Bad detections + STOP')
        self.worksheet.write(start_row, 9, self.bad_detections_and_stop)
        start_row += 1
        self.worksheet.write(start_row, 8, 'Final IoU == Max IoU')
        self.worksheet.write(start_row, 9, self.bad_final_equal_max)
        start_row += 1
        self.worksheet.write(start_row, 8, 'Average change in IoU')
        self.worksheet.write(start_row, 9, '=(SUM(K2:K' + str(self.row_bad + 1) + ')-SUM(J2:J' + str(self.row_bad + 1) + '))/' + str(num_bad_detections))

        self.workbook.close()

        print('IoU increase: ' + str(num_good_detections))
        print('IoU decrease: ' + str(num_bad_detections))
