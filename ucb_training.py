from image_helper import *
from parse_annotations import *
from reinforcement import *
from metrics import *
from visualization import *
from linUCB import LinucbAgent
from sequence_actions import *


features = np.array([])
training_checkpoint = False  # reload previously trained model
iou_threshold = 0.85
class_object = 3
number_of_steps = 10
state_type = HOG

folder_skews = 'skews_bigger'
path_model = '/home/user/morgane/drl-agent/models_image_zooms'
path_testing_folder = '/home/user/morgane/drl-agent/testing_visualizations'
path_dataset = '/home/user/morgane/datasets/verb_middle_50'


if __name__ == "__main__":

    pretrained_model = None
    visual_descriptor_size = 0
    if state_type == CONV:
        pretrained_model = obtain_compiled_vgg_16()
        visual_descriptor_size = 25088
    elif state_type == CONV_RES:
        pretrained_model = obtain_compiled_resnet50()
        visual_descriptor_size = 2048
    elif state_type == HOG:
        visual_descriptor_size = 2916
    elif state_type == CORNER:
        visual_descriptor_size = 1024
    else:
        raise ValueError('Unknown state type.')
    state_size = visual_descriptor_size + actions_of_history*number_of_actions

    # initialize the agent
    if training_checkpoint:
        path_agent = path_model + '/model.pickle'
    else:
        path_agent = "0"

    linucb = LinucbAgent(path_agent)

    # load image names
    image_names = np.array([load_images_names_in_data_set('train_25', path_dataset)])
    images = get_all_images(image_names, path_dataset)

    image_names_human = np.array([load_images_names_in_data_set('human_25', path_dataset)])
    images_human = get_all_images(image_names_human, path_dataset)

    for i in range(np.size(image_names_human)):
        human_annotation = get_bb_from_annotation(image_names_human[0][i], path_dataset + '/annotations/')
        initial_annotation = get_bb_from_annotation(image_names_human[0][i], path_dataset + '/' + folder_skews + '/')

        counters = get_sequence_actions(initial_annotation, human_annotation)
        image = np.array(images_human[i])
        image_name = image_names_human[0][i]

        region_image = image[initial_annotation[0][3]:initial_annotation[0][4],
                             initial_annotation[0][1]:initial_annotation[0][2]]
        region_mask = np.zeros([image.shape[0], image.shape[1]])
        region_mask[initial_annotation[0][3]:initial_annotation[0][4],
                    initial_annotation[0][1]:initial_annotation[0][2]] = 1
        bbox_ul = (initial_annotation[0][3], initial_annotation[0][1])
        bbox_lr = (initial_annotation[0][4], initial_annotation[0][2])

        history_vector = np.zeros([number_of_actions * actions_of_history])
        # computation of the initial state
        state = get_state(region_image, history_vector, pretrained_model, state_type, image, bbox_ul,
                          bbox_lr, visual_descriptor_size)

        step = 0
        for index, counter in enumerate(counters):
            while counter > 0:
                step += 1
                region_mask, region_image, bbox_ul, bbox_lr = perform_movement_action((image.shape[0], image.shape[1]),
                                                                                      region_image, index+1,
                                                                                      bbox_ul, bbox_lr,
                                                                                      image)
                history_vector = update_history_vector(history_vector, index+1)
                new_state = get_state(region_image, history_vector, pretrained_model, state_type, image, bbox_ul,
                                      bbox_lr, visual_descriptor_size)
                linucb.remember(state, index+1, 1)
                state = new_state
                counter -= 1
        linucb.remember(state, 9, 1)
        linucb.retrain(path_model)

    # iterate through the images
    for j in range(np.size(image_names)):
        image = np.array(images[j])
        image_name = image_names[0][j]

        print("Image name: ", image_name)
        # ground-truth bounding-box
        annotation = get_bb_from_annotation(image_name, path_dataset + '/annotations/') # 2D array, one array for each object (class number + coordinates)

        # skewed bounding-box
        skewed_annotation = get_bb_from_annotation(image_name, path_dataset + '/' + folder_skews + '/')

        gt_masks = generate_bounding_box_from_annotation(annotation, image.shape)
        array_classes_gt_objects = get_ids_objects_from_annotation(annotation)

        # Iterate through all the objects in the ground truth of an image
        for k in range(np.size(array_classes_gt_objects)):
            # We check whether the ground truth object is of the target class category
            if array_classes_gt_objects[k] == class_object:
                gt_mask = gt_masks[:, :, k]
                region_image = image[skewed_annotation[0][3]:skewed_annotation[0][4],
                                     skewed_annotation[0][1]:skewed_annotation[0][2]]
                region_mask = np.zeros([image.shape[0], image.shape[1]])
                region_mask[skewed_annotation[0][3]:skewed_annotation[0][4],
                            skewed_annotation[0][1]:skewed_annotation[0][2]] = 1
                bbox_ul = (skewed_annotation[0][3], skewed_annotation[0][1])
                bbox_lr = (skewed_annotation[0][4], skewed_annotation[0][2])
                # follow_iou function calculates at each time step which is the ground truth object
                # that overlaps more with the visual region, so that we can calculate the rewards appropriately
                iou = follow_iou(gt_mask, region_mask)
                new_iou = iou
                initial_iou = iou
                # init of the history vector that indicates past actions (9 actions * 4 steps in the memory)
                history_vector = np.zeros([number_of_actions * actions_of_history])
                # computation of the initial state
                state = get_state(region_image, history_vector, pretrained_model, state_type, image, bbox_ul,
                                  bbox_lr, visual_descriptor_size)
                # status indicates whether the agent is still alive and has not triggered the terminal action
                action = 0
                reward = 0
                preds = 0
                step = 0
                done = False
                if step > number_of_steps:  # max number of steps for one object is exceeded
                    step += 1
                while (step < number_of_steps) and (not done):
                    category = int(array_classes_gt_objects[k] - 1)
                    step += 1
                    if step == number_of_steps:
                        action = 9

                    else:
                        action = linucb.predict_action(state.T)

                    # terminal action
                    if action == 9:
                        reward = get_reward_stop(new_iou, iou_threshold)
                        done = True

                    else:
                        region_mask, region_image, bbox_ul, bbox_lr = perform_movement_action((image.shape[0], image.shape[1]),
                                                                                              region_image, action,
                                                                                              bbox_ul, bbox_lr,
                                                                                              image)
                        new_iou = follow_iou(gt_mask, region_mask)
                        reward = get_reward_movement(iou, new_iou)
                        iou = new_iou
                        done = False

                    history_vector = update_history_vector(history_vector, action)
                    new_state = get_state(region_image, history_vector, pretrained_model, state_type, image, bbox_ul,
                                          bbox_lr, visual_descriptor_size)
                    linucb.remember(state, action, reward)
                    state = new_state

                    if done:  # action == 9
                        linucb.retrain(path_model)
