import xml.etree.ElementTree as ET
import numpy as np
import os


class EmptyAnnotationFile(Exception):
    pass


def get_bb_from_annotation(name, path_dataset):
    exists_xml = os.path.isfile(path_dataset + name + '.xml')
    exists_txt = os.path.isfile(path_dataset + name + '.txt')
    if exists_xml:
        category_and_bb = get_bb_from_pascal_xml_annotation(name, path_dataset)
    elif exists_txt:
        category_and_bb = get_bb_from_retinanet_txt_annotation(name, path_dataset)
    else:
        raise FileNotFoundError
    return category_and_bb


def get_bb_from_pascal_xml_annotation(xml_name, path_labels):
    string = path_labels + xml_name + '.xml'
    tree = ET.parse(string)
    root = tree.getroot()
    names = []
    x_min = []
    x_max = []
    y_min = []
    y_max = []
    for child in root:
        if child.tag == 'object':
            for child2 in child:
                if child2.tag == 'name':
                    names.append(child2.text)
                elif child2.tag == 'bndbox':
                    for child3 in child2:
                        if child3.tag == 'xmin':
                            x_min.append(child3.text)
                        elif child3.tag == 'xmax':
                            x_max.append(child3.text)
                        elif child3.tag == 'ymin':
                            y_min.append(child3.text)
                        elif child3.tag == 'ymax':
                            y_max.append(child3.text)
    category_and_bb = np.zeros([np.size(names), 5])
    for i in range(np.size(names)):
        category_and_bb[i][0] = int(get_id_of_class_name(names[i]))
        category_and_bb[i][1] = x_min[i]
        category_and_bb[i][2] = x_max[i]
        category_and_bb[i][3] = y_min[i]
        category_and_bb[i][4] = y_max[i]
    return category_and_bb.astype(int)


def get_bb_from_retinanet_txt_annotation(name, path_dataset):
    string = path_dataset + name + '.txt'
    if os.stat(string).st_size == 0:
        return np.zeros([1, 5])
    with open(string) as f:
        content = f.readlines()
    category_and_bb = np.zeros([1, 5])
    conf = 0
    for i in range(content.__len__()):
        annotation = content[i].split(' ')
        category_and_bb[0][0] = annotation[0]
        if float(annotation[1]) > conf:
            conf = float(annotation[1])
            category_and_bb[0][1] = annotation[2]
            category_and_bb[0][2] = annotation[4]
            category_and_bb[0][3] = annotation[3]
            category_and_bb[0][4] = annotation[5]
    return category_and_bb.astype(int)


def get_all_annotations(image_names, path_vor):
    annotations = []
    for i in range(np.size(image_names)):
        image_name = image_names[0][i]
        annotations.append(get_bb_from_pascal_xml_annotation(image_name, path_vor))
    return annotations


def generate_bounding_box_from_annotation(annotation, image_shape):
    length_annotation = int(annotation.shape[0])
    masks = np.zeros([image_shape[0], image_shape[1], length_annotation])
    for i in range(0, length_annotation):
        masks[int(annotation[i, 3]):int(annotation[i, 4]), int(annotation[i, 1]):int(annotation[i, 2]), i] = 1
    return masks


def generate_annotation_from_bounding_box(class_object, image_name, bbox_ul, bbox_lr, path_save):
    if not os.path.exists(os.path.dirname(path_save + '/new_annotations/')):
        os.makedirs(os.path.dirname(path_save + '/new_annotations/'))
    with open(path_save + '/new_annotations/' + image_name + '.txt', 'a') as file:
        file.write(str(class_object) + ' ' + str(bbox_ul[1]) + ' ' + str(bbox_ul[0]) + ' ' + str(bbox_lr[1]) + ' ' + str(bbox_lr[0]) + '\n')


def get_ids_objects_from_annotation(annotation):
    return annotation[:, 0]


def get_id_of_class_name(class_name):
    if class_name == 'Verbandskasten_right':
        return 1
    elif class_name == 'Verbandskasten_left':
        return 2
    elif class_name == 'Verbandskasten_middle':
        return 3
    elif class_name == 'Riss':
        return 4
    elif class_name == 'airbag_sticker':
        return 5
    elif class_name == 'aeroplane':
        return 6
    else:
        raise ValueError('Unknown class.')
