from dqn_agent import DQNAgent
from image_helper import *
from parse_annotations import *
from reinforcement import *
from metrics import *
from visualization import *

epochs_id = 0
epochs = 80
iou_threshold = 0.7
class_object = 6
number_of_steps = 10
batch_size = 32
state_type = CONV_RES

folder_skews = 'retinanet_detections_trainval2012_50'
path_model = '/home/user/morgane/drl-agent/models_image_zooms'
path_testing_folder = '/home/user/morgane/drl-agent/testing_visualizations'
path_dataset = '/home/user/morgane/datasets/aeroplane_2007'

if __name__ == "__main__":

    pretrained_model = None
    visual_descriptor_size = 0
    if state_type == CONV:
        pretrained_model = obtain_compiled_vgg_16()
        visual_descriptor_size = 25088
    elif state_type == CONV_RES:
        pretrained_model = obtain_compiled_resnet50()
        visual_descriptor_size = 2048
    elif state_type == HOG:
        visual_descriptor_size = 2916
    elif state_type == CORNER:
        visual_descriptor_size = 1024
    else:
        raise ValueError('Unknown state type.')
    state_size = visual_descriptor_size + actions_of_history*number_of_actions

    if epochs_id == 0:
        weights = "0"
    else:
        weights = path_model + '/model' + str(class_object) + '.h5'

    # initialize the agent
    agent = DQNAgent(state_size, 9, weights)
    if not epochs_id == 0:
        agent.epsilon = 0.1

    # load image names
    image_names = np.array([load_images_names_in_data_set('train', path_dataset)])
    images = get_all_images(image_names, path_dataset)

    # iterate through the images
    for i in range(epochs_id, epochs_id + epochs):
        if agent.epsilon >= (agent.epsilon_min + agent.epsilon_decay):
            agent.epsilon = round(1.0 - i*agent.epsilon_decay, 2)
        print(agent.epsilon)
        iou_count = 0
        iou_avg = 0
        for j in range(np.size(image_names)):
            image = np.array(images[j])
            image_name = image_names[0][j]
            
            # ground-truth bounding-box
            annotation = get_bb_from_annotation(image_name, path_dataset + '/annotations/') # 2D array, one array for each object (class number + coordinates)

            # skewed bounding-box
            skewed_annotation = get_bb_from_annotation(image_name, path_dataset + '/' + folder_skews + '/')

            print("Image name: ", image_name)

            # 3D array, every layer corresponds to one object in the image, area of objects masked (=1)
            gt_masks = generate_bounding_box_from_annotation(annotation, image.shape)
            # array of class id of every object in image
            array_classes_gt_objects = get_ids_objects_from_annotation(annotation)
            array_classes_skew_objects = get_ids_objects_from_annotation(skewed_annotation)

            # Iterate through all the objects in the ground truth of an image
            for k in range(np.size(array_classes_skew_objects)):
                # We check whether the ground truth object is of the target class category
                if array_classes_skew_objects[k] == class_object:
                    region_image = image[skewed_annotation[k][3]:skewed_annotation[k][4],
                                         skewed_annotation[k][1]:skewed_annotation[k][2]]
                    region_mask = np.zeros([image.shape[0], image.shape[1]])
                    region_mask[skewed_annotation[k][3]:skewed_annotation[k][4],
                                skewed_annotation[k][1]:skewed_annotation[k][2]] = 1
                    gt_mask = determine_gt(gt_masks, region_mask, np.size(array_classes_gt_objects))
                    # gt_mask = gt_masks[:, :, k]
                    bbox_ul = (skewed_annotation[k][3], skewed_annotation[k][1])
                    bbox_lr = (skewed_annotation[k][4], skewed_annotation[k][2])
                    # calculate iou
                    iou = follow_iou(gt_mask, region_mask)
                    new_iou = iou
                    initial_iou = iou
                    # init of the history vector that indicates past actions (9 actions * 4 steps in the memory)
                    history_vector = np.zeros([number_of_actions*actions_of_history])
                    # computation of the initial state
                    state = get_state(region_image, history_vector, pretrained_model, state_type, image, bbox_ul,
                                      bbox_lr, visual_descriptor_size)
                    action = 0
                    reward = 0
                    done = False
                    is_random = False
                    step = 0
                    while (step < number_of_steps) and (not done):
                        step += 1
                        if step == number_of_steps:
                            action = 9
                        elif (np.round(new_iou, 2) >= (iou_threshold + 0.05)) and (np.random.rand() <= 0.5):
                            action = 9
                        # epsilon-greedy policy
                        else:
                            action, is_random = agent.act(state.T)
                            action += 1

                        # terminal action
                        if action == 9:
                            done = True
                            reward = get_reward_trigger(iou, iou_threshold, step, number_of_steps)
                            iou_avg = float(((iou_avg*iou_count)+new_iou)/(iou_count+1))
                            iou_count += 1
                        else:
                            region_mask, region_image, bbox_ul, bbox_lr = perform_movement_action((image.shape[0], image.shape[1]),
                                                                                                  region_image, action,
                                                                                                  bbox_ul, bbox_lr,
                                                                                                  image)
                            new_iou = follow_iou(gt_mask, region_mask)
                            reward = get_reward_movement(iou, new_iou)
                            iou = new_iou
                            done = False
  
                        history_vector = update_history_vector(history_vector, action)
                        new_state = get_state(region_image, history_vector, pretrained_model, state_type, image, bbox_ul,
                                              bbox_lr, visual_descriptor_size)
                        agent.remember(state.T, action-1, reward, new_state.T, done)
                        state = new_state

                        if done:
                            if len(agent.memory) >= batch_size:
                                agent.replay(batch_size)

        agent.on_epoch_end(i, iou_avg, path_model)
        
        model = agent.model
        string = path_model + '/model' + str(class_object) + '.h5'
        model.save_weights(string, overwrite=True)
