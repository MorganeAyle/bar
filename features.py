import keras
from keras.applications.vgg16 import VGG16
from keras.applications.vgg16 import preprocess_input as vgg16_preprocess
from keras.applications.resnet50 import ResNet50
from keras.applications.resnet50 import preprocess_input as resnet_preprocess
from keras.layers import Flatten, Dense, Dropout, AveragePooling2D
from keras.models import Model
from keras.preprocessing import image
from skimage import feature
import cv2
import numpy as np


def get_HOG_image_descriptor_for_image(region_image):
    im = cv2.resize(region_image, (224, 224))
    im = cv2.bilateralFilter(im, 11, 17, 17)
    (H, hog_image) = feature.hog(im, orientations=9, pixels_per_cell=(12, 12),
                                 cells_per_block=(1, 1), transform_sqrt=True, block_norm="L2", visualize=True)
    # hog_image = exposure.rescale_intensity(hog_image, out_range=(0, 255))
    # hog_image = hog_image.astype("uint8")
    return H


def get_CORNER_image_descriptor_for_image(image):
    im = cv2.resize(image, (32, 32))
    gray_image = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    H = feature.corner_fast(image=gray_image, n=2, threshold=0.005)
    return H


def get_conv_image_descriptor_for_image(region_image, model):
    img = cv2.resize(region_image, (224, 224))
    img_data = image.img_to_array(img)
    img_data = np.expand_dims(img_data, axis=0)
    img_data = vgg16_preprocess(img_data)
    vgg16_feature = model.predict(img_data)
    return vgg16_feature


def get_res_image_descriptor(region_image, model):
    img = cv2.resize(region_image, (224, 224))
    img_data = image.img_to_array(img)
    img_data = np.expand_dims(img_data, axis=0)
    img_data = resnet_preprocess(img_data)
    resnet50_feature = model.predict(img_data)
    return resnet50_feature


def obtain_compiled_vgg_16():
    keras.backend.set_image_data_format('channels_last')
    model = VGG16(include_top=False, weights='imagenet', input_shape=(224, 224, 3))
    return model


def obtain_compiled_resnet50():
    keras.backend.set_image_data_format('channels_last')
    model = ResNet50(include_top=False, weights='imagenet', input_shape=(224, 224, 3))
    model.summary()
    x = model.get_layer('activation_43').output
    feature_vector = AveragePooling2D((7, 7))(x)
    extractor = Model(inputs=[model.input], outputs=[feature_vector])
    return extractor
