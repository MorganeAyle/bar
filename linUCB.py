import numpy as np
import pickle
from contextualbandits.online import LinUCB


class LinucbAgent:
    def __init__(self, path_agent):
        self.agent = self.initialize_agent(path_agent)
        self.rewards = np.array([])
        self.actions = np.array([])
        self.features = np.array([])

    def initialize_agent(self, path_agent):
        if path_agent == "0":
            linucb = LinUCB(nchoices=9, alpha=1)
        else:
            with open(path_agent, 'rb') as outfile:
                linucb = pickle.load(outfile)
        return linucb

    def predict_action(self, state):
        action = self.agent.predict(state)
        action += 1
        return action

    def remember(self, state, action, reward):
        self.rewards = np.append(self.rewards, reward)
        self.actions = np.append(self.actions, action - 1)
        if self.features.size == 0:
            self.features = np.append(self.features, state)
        else:
            self.features = np.vstack((self.features, state.T))

    def empty_memory(self):
        self.rewards = np.array([])
        self.actions = np.array([])
        self.features = np.array([])

    def retrain(self, model_path):
        print("Refitting to data...")
        self.agent.partial_fit(self.features, self.actions, self.rewards)
        self.empty_memory()

        with open(model_path + '/model.pickle', 'wb') as outfile:
            pickle.dump(self.agent, outfile)
