from keras.models import Sequential
from keras.layers.core import Dense
from keras.optimizers import Adam
import numpy as np
import random
from collections import deque
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


# Deep Q-learning Agent
class DQNAgent:
    def __init__(self, state_size, action_size, weights):
        self.DDQN = False
        self.state_size = state_size
        self.action_size = action_size
        self.memory = deque(maxlen=2000)
        self.memory_human = deque(maxlen=2000)
        self.gamma = 0.90    # discount rate
        self.epsilon = 1.0  # exploration rate
        self.epsilon_min = 0.1
        self.epsilon_decay = 0.06
        self.learning_rate = 0.001
        self.model = self._build_model(weights)
        self.target_model = self.model
        self.losses = []
        self.x = []
        self.i = 0
        self.q_avg = 0
        self.q_count = 0
        self.q_stop_count = 0
        self.q_stop_avg = 0
        self.epochs = 1
        self.q_list = []
        self.q_stop_list = []
        self.iou_list = []

    def _build_model(self, weights):
        # Neural Net for Deep-Q learning Model
        model = Sequential()
        model.add(Dense(500, input_dim=self.state_size, activation='relu', kernel_initializer='random_normal'))
        model.add(Dense(500, input_dim=self.state_size, activation='relu', kernel_initializer='random_normal'))
        model.add(Dense(self.action_size, activation='linear', kernel_initializer='random_normal'))
        model.compile(loss='mse', optimizer=Adam(lr=self.learning_rate), metrics=['acc'])
        if weights != "0":
            model.load_weights(weights)
            print("Weights loaded.")
        return model

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def act(self, state):
        if np.random.rand() <= self.epsilon:
            return random.randrange(self.action_size), True
        act_values = self.model.predict(state)
        action = np.argmax(act_values[0])
        if not action == self.action_size - 1:  # last action = stop action
            self.q_avg = float(((self.q_avg*self.q_count)+np.max(act_values[0]))/(self.q_count+1))
            self.q_count += 1
        else:
            self.q_stop_avg = float(((self.q_stop_avg*self.q_stop_count)+np.max(act_values[0]))/(self.q_stop_count+1))
            self.q_stop_count += 1  
        return np.argmax(act_values[0]), False  # returns action

    def replay(self, batch_size):
        minibatch = random.sample(self.memory, batch_size)
        targets = np.zeros((len(minibatch), self.action_size))
        for i in range(len(minibatch)):
            state, action, reward, next_state, done = minibatch[i]
            target = reward
            if not done:
                if self.DDQN:
                    target = reward + self.gamma * self.target_model.predict(next_state)[0][np.argmax(self.model.predict(next_state)[0])]
                else:
                    target = reward + self.gamma * np.amax(self.target_model.predict(next_state)[0])
            targets[i, :] = self.model.predict(state)
            targets[i][action] = target
        x = np.concatenate([state for (state, action, reward, next_state, done) in minibatch])
        history = self.model.fit(x, targets, epochs=1, verbose=0)
        self.losses.append(history.history['loss'])
        self.x.append(self.i)
        self.i += 1
        # update target network every C=500 steps
        if self.i % 500 == 0:
            self.target_model = self.model

    def on_epoch_end(self, epoch, iou_avg, path_models):
        plt.plot(self.x, self.losses, label='loss')
        plt.xlabel('episodes')
        plt.ylabel('loss')
        plt.legend()
        plt.savefig(path_models + '/loss_plot_' + str(epoch) + '.png')
        plt.clf()
        self.q_list.append(self.q_avg)
        plt.plot(list(range(0, self.epochs)), self.q_list, label='q_avg')
        plt.legend()
        self.q_stop_list.append(self.q_stop_avg)
        plt.plot(list(range(0, self.epochs)), self.q_stop_list, label='q_stop_avg')
        plt.legend()
        plt.xlabel('epochs')
        plt.ylabel('average predicted q value')
        plt.savefig(path_models + '/q_plot_' + str(epoch) + '.png')
        plt.clf()
        self.q_avg = 0
        self.q_count = 0
        self.q_stop_avg = 0
        self.q_stop_count = 0
        self.iou_list.append(iou_avg)
        plt.plot(list(range(0, self.epochs)), self.iou_list, label='iou_avg')
        plt.legend()
        plt.xlabel('epochs')
        plt.ylabel('average final iou')
        plt.savefig(path_models + '/iou_plot_' + str(epoch) + '.png')
        plt.clf()
        self.epochs += 1
