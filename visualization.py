from PIL import Image, ImageFont
import numpy as np

path_font = "/usr/share/fonts/liberation/LiberationMono-Regular.ttf"
font = ImageFont.truetype(path_font, 12)


def string_for_action(action):
    if action == 0:
        return 'START'
    if action == 1:
        return 'top'
    elif action == 2:
        return 'down'
    elif action == 3:
        return 'left'
    elif action == 4:
        return 'right'
    elif action == 5:
        return 'wider'
    elif action == 6:
        return 'taller'
    elif action == 7:
        return 'fatter'
    elif action == 8:
        return 'thinner'
    elif action == 9:
        return 'STOP'


def draw_sequences(step, action, draw, region_image, background, path_testing_folder, iou, initial_iou, image_name):

    offset = 138
    image_offset = (offset + 500 * step, 100)
    text_offset = (offset + 500 * step, 350)
    action_string = string_for_action(action)
    footnote = 'Action: ' + action_string + ' ' + '\nIoU: ' + str(iou)
    # draw footnote
    draw.text(text_offset, str(footnote), (0, 0, 0), font=font)
    img_for_paste = Image.fromarray(region_image).resize((224, 224))
    background.paste(img_for_paste, image_offset)
    if (action == 9) & (np.round(iou, 2) >= initial_iou):
        background.save(path_testing_folder + '/good_detection/' + image_name + '.png')
    elif (action == 9) & (np.round(iou, 2) < initial_iou):
        background.save(path_testing_folder + '/bad_detection/' + image_name + '.png')
    return background
