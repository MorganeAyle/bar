import numpy as np
import cv2

iou_list = []
episodes = 0


def calculate_iou(img_mask, gt_mask):
    gt_mask *= 1.0
    img_and = cv2.bitwise_and(img_mask, gt_mask)
    img_or = cv2.bitwise_or(img_mask, gt_mask)
    j = np.count_nonzero(img_and)
    i = np.count_nonzero(img_or)
    iou = float(float(j)/float(i))
    return iou


def calculate_overlapping(img_mask, gt_mask):
    gt_mask *= 1.0
    img_and = cv2.bitwise_and(img_mask, gt_mask)
    j = np.count_nonzero(img_and)
    i = np.count_nonzero(gt_mask)
    overlap = float(float(j)/float(i))
    return overlap


def follow_iou(gt_mask, region_mask):
    iou = calculate_iou(gt_mask, region_mask)
    return iou


def determine_gt(gt_masks, region_mask, num_gt):
    max_overlap = 0
    max_gt = 0
    for i in range(0, num_gt):
        gt_mask = gt_masks[:, :, i]
        overlap = calculate_overlapping(region_mask, gt_mask)
        if i == 0:
            max_overlap = overlap
        else:
            if overlap > max_overlap: 
                max_overlap = overlap
                max_gt = i
    return gt_masks[:, :, max_gt]
